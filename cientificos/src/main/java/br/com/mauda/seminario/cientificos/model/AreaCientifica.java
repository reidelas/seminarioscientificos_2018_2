package br.com.mauda.seminario.cientificos.model;

public class AreaCientifica {

    private Long id;
    private String nome;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public AreaCientifica(Long id, String nome) {
        super();
        this.id = id;
        this.nome = nome;
    }

}
