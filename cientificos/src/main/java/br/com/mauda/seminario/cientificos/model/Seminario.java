package br.com.mauda.seminario.cientificos.model;

import java.util.Date;

public class Seminario {

    private long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public Seminario(long id, String titulo, String descricao, Boolean mesaRedonda, Date data, Integer qtdInscricoes) {
        super();
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
        this.mesaRedonda = mesaRedonda;
        this.data = data;
        this.qtdInscricoes = qtdInscricoes;
    }

}
