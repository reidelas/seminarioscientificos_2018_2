package br.com.mauda.seminario.cientificos.model;

public class Professor {

    private long id;
    private String email;
    private String nome;
    private double salario;
    private String telefone;

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getSalario() {
        return this.salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Professor(long id, String email, String nome, double salario, String telefone) {
        super();
        this.id = id;
        this.email = email;
        this.nome = nome;
        this.salario = salario;
        this.telefone = telefone;
    }

}
