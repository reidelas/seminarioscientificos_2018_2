package br.com.mauda.seminario.cientificos.model;

public class Curso {

    private long id;
    private String nome;

    public Curso(long id, String nome) {
        super();
        this.id = id;
        this.nome = nome;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
